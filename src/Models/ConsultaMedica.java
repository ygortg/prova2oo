package Models;

public class ConsultaMedica {
	
	private String diagnostico, data, hora;
	private Medico umMedico;
	private Paciente umPaciente;
	public ConsultaMedica(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Medico getUmMedico() {
		return umMedico;
	}

	public void setUmMedico(Medico umMedico) {
		this.umMedico = umMedico;
	}

	public Paciente getUmPaciente() {
		return umPaciente;
	}

	public void setUmPaciente(Paciente umPaciente) {
		this.umPaciente = umPaciente;
	}
	

}
